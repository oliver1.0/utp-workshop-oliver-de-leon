import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/commons/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  //private usersEndpoint = environment.usersEndpoint;
  private usersEndpoint = environment.lambdaUserEndpoint;

  constructor(private http: HttpClient) { }

  getUsers(): Observable <User[]> {
    return this.http.get(`${this.usersEndpoint}/users`).pipe(
      map((r: any) => {
       // Operaciones generales sobre los datos
        console.log(`Desde el Servicio   | longitud de data: ${r.length}`);

        return r;
      })
    );
  }

}
