import { Component } from '@angular/core';
import { User } from '../commons/interfaces/user';
import { UsersService } from '../services/api/users.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  User!: User[];

  constructor(private getUserService: UsersService) {}

  ngOnInit(): void {
    this.getUserService.getUsers()
    .subscribe((response: User[]) => {

      // Logica especifica del componente
      response.forEach(r => {
        console.log(`Desde el Componente | Ciudad: ${r.address?.city}`);
      });

      this.User = response;

    });
  }

  userSelected(user: User): void {
    console.log(user.id);
  }

}
