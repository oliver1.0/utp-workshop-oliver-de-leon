import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
})
export class ListItemsComponent  implements OnInit {

  @Input() items!: any[];
  @Output() itemSelected = new EventEmitter<any>();
  
  tittle = 'Contactos';

  constructor() {}
  ngOnInit(): void {}

  selectedItem(item: any): void {
    this.itemSelected.emit(item);
  }

}
