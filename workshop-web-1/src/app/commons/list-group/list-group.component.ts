import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-list-group',
  templateUrl: './list-group.component.html',
  styleUrls: ['./list-group.component.css']
})
export class ListGroupComponent {

  @Input() items!: any[];
  @Output() itemSelected = new EventEmitter<any>();
  
  tittle = 'Listado de usuarios';

  constructor() {}

  selectedItem(item: any): void {
    this.itemSelected.emit(item);
  }

}
